import styled from 'styled-components'//
import { 
    CardIcon, 
    ImageBigCard, 
    ImageLittleLeftCard, 
    ImageLittleRightCard } from './Image'
import { Text } from './Text'

// Big Card
const CardComponent = styled.section`
    border: ${props => props.border};
    height: auto;
    border-radius: 15px;
    display: grid;
    align-items: center;
    grid-auto-flow: column;
    
    @media screen and (max-width: 1024px) {
        padding-top: 5px;
        width:70%;
        margin-bottom:10px;
        align-items: center;
        justify-content: center;
    }
    @media screen and (max-width:768px) {
        padding-top: 15px;
        width:90%;
        margin-bottom:10px;
        align-items: center;
        justify-content: center;
        grid-template-rows: repeat(2,minmax(150px,auto));
    }
    @media screen and (max-width:320px) {
        padding-top: 15px;
        width:100%;
        margin-bottom:10px;
        align-items: center;
        justify-content: center;
        grid-template-rows: repeat(1,minmax(150px,auto));
    }

`
const CardSection = styled.section`
    
        justify-self: start;
        display: grid;
        align-items: center;
        justify-content: center;
        width: 20.5em;
        height: 12rem;
        margin: 0 auto;
`
const CardMenuSection = styled.section`
    justify-self: start;
    display:grid;
    align-items: center;
    justify-content: center;
    grid-template-areas:'One Two Four Six'
                        'One Three Five Seven';
    grid-template-rows: repeat(3, minmax(70px, auto));
    column-gap: 10px;
    row-gap: 10px;
    padding-top: 20px;
    width: ${props => props.width};
    height: ${props => props.height};

    @media screen and (max-width: 1024px) {
        width:100%;
        height:auto;
        grid-template-areas:'One One'
                            'Two Three '
                            'Four Six'
                            'Five Seven';
        grid-template-rows: repeat(4, minmax(4.375rem, auto));
        padding:30px;
        padding-left:30px;
    }
    @media screen and (max-width: 320px) {
        width:100%;
        height:auto;
        grid-template-areas:'One'
                            'One '
                            'Two'
                            'Three'
                            'Four'
                            'Six'
                            'Five'
                            'Seven';
        grid-template-rows: repeat(4, minmax(4.375rem, auto));
        padding:30px;
        padding-left:30px;
    }
`
const BigCardIcon = styled.div`
    grid-area: ${props => props.gridArea};
    background-color: #099CB0;
    border-radius: 10px;
    display: grid;
    justify-items: center;
    align-items: center;
    width: auto;
    height: 150px;
    padding: 10px;
    cursor: pointer;
    
`
const BigNormalCardIcon = styled.div`
    grid-area: ${props => props.gridArea};
    background-color: ${props => props.backgroundColor};
    border-radius: 10px;
    display: grid;
    justify-content: center;
    align-items: center;
    grid-auto-flow: column dense;
    column-gap: 10px;
    width: auto;
    height: 70px;
    cursor: pointer;

`
export const BigCard = (props)=>{
    return(
        <CardComponent 
            border='solid 4px #099CB0'>
            <CardSection>
                <ImageBigCard src={props.image}/> 
                <Text
                    title={props.bigCardTextImage}
                    fontFamily='Montserrat-Regular'
                    color='white'
                    fontSize='36px'/>
            </CardSection>
            <CardMenuSection width='880px' height='192px'>
                <BigCardIcon gridArea='One' onClick={props.bigCard.link}>
                    <CardIcon 
                        src={props.bigCard.icon}
                        width='70px'
                        height='70px'/>
                    <Text 
                        title={props.bigCard.title}
                        fontFamily='Montserrat-Regular'
                        color='white'
                        fontSize='2.3rem'/>
                    <Text 
                        title={props.bigCard.subTitle}
                        fontFamily='Montserrat-Regular'
                        color='white'
                        fontSize='16px'/>
                </BigCardIcon>
                {props.bigNormalCards.map((bigNormaCard)=>(
                    <BigNormalCardIcon
                        key={bigNormaCard.id}
                        gridArea={bigNormaCard.gridArea}
                        backgroundColor='#099CB0'>
                        <Text 
                            title={bigNormaCard.title}
                            fontFamily='Montserrat-Regular'
                            color='white'
                            fontSize='1.375rem'/>
                        <CardIcon
                            src={bigNormaCard.icon}
                            width='44px'
                            height='38px'/>
                    </BigNormalCardIcon>
                ))}
            </CardMenuSection>
        </CardComponent>
    )
}
// Little Cards
const CardComponentRight = styled.section`
    border: ${props => props.border};
    width: 37.625rem;
    height: auto;
    border-radius: 15px;
    display: grid;
    align-items: center;
    justify-content: center;
    grid-auto-flow: column;
    column-gap: 20px;
    @media screen and (max-width: 768px) {
        padding-top: 5px;
        width:90%;
        align-items: center;
        justify-content: center;
        grid-template-rows: repeat(3,minmax(1rem,auto));
        margin-bottom:10px;
    }
`

const CardComponentLeft = styled.section`
    border: ${props => props.border};
    width: 37.625rem;
    height: auto;
    border-radius: 15px;
    display: grid;
    align-items: center;
    justify-content: center;
    grid-auto-flow: column;
    column-gap: 20px;
    @media screen and (max-width: 768px) {
        padding-top: 15px;
        width:90%;
        align-items: center;
        justify-content: center;
        grid-template-rows: repeat(3,minmax(1rem,auto));
        margin-bottom:20px;
    }

`

const CardContainerComponent = styled.div`
    display: inline-grid;
    grid-auto-flow: column;
    justify-content: center;
    align-items: center;
    column-gap: 20px;
    width: 100%;
    margin-left:35px;
    @media screen and (max-width: 1024px) {
        grid-template-rows: repeat(2,minmax(15rem,1fr));
        
    }
`
const LittleImageCardSection = styled.section`
    justify-self: start;
    display: grid;
    align-items: center;
    justify-content: center;
    width: 172px;
    height: 192px;
    margin: 0 auto;
`
const CardLeftMenuSection = styled.section`
    justify-self: start;
    display:grid;
    align-items: center;
    justify-content: center;
    grid-template-areas:'One Two'
                        'One Three';
    grid-template-rows: repeat(2, minmax(70px, auto));
    column-gap: 10px;
    row-gap: 10px;
    width: ${props => props.width};
    height: ${props => props.height};
    @media screen and (max-width: 768px) {
        height:auto;
        grid-template-areas:'One One'
                            'Two Three ';
        padding:10px;
        padding-left:10px;
    }
`
const CardRightMenuSection = styled.section`
    justify-self: start;
    display:grid;
    align-items: center;
    justify-content: center;
    grid-template-areas:'One Three'
                        'Two Three';
    grid-template-rows: repeat(2, minmax(70px, auto));
    column-gap: 10px;
    row-gap: 10px;
    width: ${props => props.width};
    height: ${props => props.height};

    @media screen and (max-width: 768px) {
        height:auto;
        grid-template-areas:'Three Three'
                            'One Two';
        padding:10px;
        padding-left:10px;
    }
`
const NormlaCardIcon = styled.div`
    grid-area: ${props => props.gridArea};
    background-color: ${props => props.backgroundColor};
    border-radius: 10px;
    display: grid;
    justify-items: center;
    align-items: center;
    width: auto;
    height: 150px;
    padding: 10px;
    cursor: pointer;
`
export const LittleCard = (props)=>{
    return(
        <CardContainerComponent>
            <CardComponentLeft
                border='solid 4px #005F98'>
                <LittleImageCardSection>
                    <ImageLittleLeftCard src={props.leftImage}/> 
                    <Text
                        title={props.leftTextImage}
                        fontFamily='Montserrat-Regular'
                        color='white'
                        fontSize='2.25rem'/>
                </LittleImageCardSection>
                <CardLeftMenuSection>
                    <NormlaCardIcon 
                        backgroundColor='#005F98'
                        gridArea='One' 
                        onClick={props.leftCard.link}>
                        <Text 
                            title={props.leftCard.title}
                            fontFamily='Montserrat-Bold'
                            color='white'
                            fontSize='1.375rem'/>
                        <CardIcon 
                            src={props.leftCard.icon}
                            width='100px'
                            height='100px'/>
                    </NormlaCardIcon>
                    {props.leftNormalCards.map((leftNormalCard)=>(
                        <BigNormalCardIcon
                            key={leftNormalCard.id}
                            gridArea={leftNormalCard.gridArea}
                            backgroundColor='#005F98'>
                            <Text 
                                title={leftNormalCard.title}
                                fontFamily='Montserrat-Regular'
                                color='white'
                                fontSize='1.375rem'/>
                            <CardIcon
                                src={leftNormalCard.icon}
                                width='44px'
                                height='38px'/>
                        </BigNormalCardIcon>
                    ))}
                </CardLeftMenuSection>
            </CardComponentLeft>
            <CardComponentRight
                border='solid 4px #08A68E'>
                <CardRightMenuSection>
                    {props.rightNormalCards.map((rightNormalCard)=>(
                        <BigNormalCardIcon
                            key={rightNormalCard.id}
                            gridArea={rightNormalCard.gridArea}
                            backgroundColor='#08A68E'>
                            <Text 
                                title={rightNormalCard.title}
                                fontFamily='Montserrat-Regular'
                                color='white'
                                fontSize='1.375rem'/>
                            <CardIcon
                                src={rightNormalCard.icon}
                                width='44px'
                                height='38px'/>
                        </BigNormalCardIcon>
                    ))}
                    <NormlaCardIcon 
                        backgroundColor='#08A68E'
                        gridArea='Three' 
                        onClick={props.rightCard.link}>
                        <CardIcon 
                            src={props.rightCard.icon}
                            width='150px'
                            height='40px'/>
                        <Text 
                            title={props.rightCard.title}
                            fontFamily='Montserrat-Bold'
                            color='white'
                            fontSize='0.875rem'/>
                    </NormlaCardIcon>
                </CardRightMenuSection>
                <LittleImageCardSection width='602px' height='192px'>
                    <ImageLittleRightCard src={props.rightImage}/> 
                    <Text
                        title={props.rightTextImage}
                        fontFamily='Montserrat-Regular'
                        color='white'
                        fontSize='2.25rem'/>
                </LittleImageCardSection>
            </CardComponentRight>
        </CardContainerComponent>
    )
}
