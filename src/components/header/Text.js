import styled from 'styled-components'

export const Text = styled.p`
    font-family: 'Montserrat-Extra-Bold';
    font-size: 22px;
    color: whitesmoke;
    z-index: 2;
    /*@media screen and (max-width: 1024px) {
        width:1024px;
        height:auto;
    }*/
`